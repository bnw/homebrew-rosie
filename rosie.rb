# typed: false
# frozen_string_literal: true

# ^^^ The 'typed: false' satisfies 'brew audit' which uses Sorbet.
#     The frozen_string_literal line prevents some future Ruby version breakage.

# Homebrew formula for Rosie Pattern Language
# https://gitlab.com/rosie-community/homebrew-rosie.git
# This line cannot be blank, else Rubocop complains about lack of class comment.
class Rosie < Formula
  desc "Text-matching pattern language and pattern matching engine"
  homepage "https://rosie-lang.org"
  url "https://gitlab.com/rosie-pattern-language/rosie.git",
      { tag:      "v1.4.0",
        revision: "c20c38f3d601a42a9f5536034f024836b78f5278" }
  head "https://gitlab.com/rosie-pattern-language/rosie.git",
       { branch: "master" }
  depends_on xcode: :build
  depends_on "readline"

  def install
    ENV.deparallelize
    # Expect 'prefix' to be something like /usr/local or /opt/homebrew
    odie "Brew install directory prefix is not set!" if prefix==""
    system "make", "clean"
    system "make", "BREW=true", "DESTDIR=#{prefix}"
    system "make", "install", "BREW=true", "DESTDIR=#{prefix}"
    ohai "Rosie installed successfully!"
    ohai "    RPL libraries, documentation, etc are in #{HOMEBREW_PREFIX}/lib/rosie"
    ohai "    Executable will be linked (by brew) to #{HOMEBREW_PREFIX}/bin/rosie"
    ohai "    Try this example, and look for color text output:"
    ohai "        rosie match all.things #{HOMEBREW_PREFIX}/lib/rosie/README"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    assert_equal "1.4.0", shell_output("#{bin}/rosie", "version").strip
    system "#{bin}/rosie", "match", "-o", "line", "all.things", "#{prefix}/README"
    #assert_equal version, shell_output("#{bin}/rosie version").strip[0, version.to_s.length]
  end
end
